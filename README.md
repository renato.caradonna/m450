# m450 - Applikationen Testen



## Ziele

Der Lernende erstellt anhand einer Testbasis ein Testkonzept und leitet daraus Testfälle ab. 
Implementiert und dokumentiert diese, definiert Korrekturmassnahmen und 
überprüft Schnittstellen anhand von Sicherheitsvorgaben.

## Mögliche Umsetzung

Das Modul wird in einem Theorie-Block und in einen Projekt-Block aufgeteilt.
Im Theorie-Block werden die Themen mit Übungen und Werkstattaufträgen vertieft.
Das erreichte Wissen wird in einem Projekt angewandt. 


## Miro-Board als Vorlage
Wir haben die Themenblöcke in einem Miro-Board abgebildet. Ähnlich zum HTML-Modul 293, erarbeiten die Lernenden die Themen zu zweit und machen dazu Übungen.
In einem zweiten Teil arbeiten sie an einem Projekt und erhalten weiterhin Inputs zu Themen, die im Projekt massgebend sind.
Miro-Board: https://miro.com/app/board/uXjVMzgn8ss=/ 

## Modul Verantwortliche

Jonas Van Essen <br>
Julian Käser


## Links

ICT Modul-Id: https://www.modulbaukasten.ch/module/450/1/de-DE?title=Applikationen-testen <br>
ZH Modulentwicklung: https://gitlab.com/modulentwicklungzh/cluster-api/m450 


