![TBZ Logo](../../x_gitres/tbz_logo.png)


<!-- TOC -->
<!-- TOC -->

# Test Driven Development

---

<img src="x_gitres/tdd.png"  width="70%" alt="Testarten">

## Glossar:

| Abkürzung | Erklärung               |
|-----------|-------------------------|
| TDD       | Test Driven Development |

## Lernziel:

* Ich kenne die Grundlagen von TDD

---

## Einführung

Test Driven Development ist keine neue Idee, sondern existiert etwa seit 2003. TDD ist ein wichtiger Aspekt von [Agile
Programming](https://en.wikipedia.org/wiki/Agile_software_development).

Ron Jeffries (einem Coach von [Extreme Programming](https://en.wikipedia.org/wiki/Extreme_programming)) verspicht einem "Clean code that works", sofern man TDD regelmässig trainiert und es konsequent und gründlich anwendet.

TDD Mantra: 