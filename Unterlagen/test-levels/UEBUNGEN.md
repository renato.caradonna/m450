# Aufgaben

* Bilden Sie zweier Gruppen
* Fassen Sie ihre Erkentnisse in einem Markdown-Dokument zusammen

## Aufgabe 1

* Diskutieren Sie: Was wird wie in ihrer Firma getestet
    * Mit welchen Test Levels hatten Sie bereits zu tun
    * Wann werden Tests ausgeführt
    * Haben Sie dedizierte Testing oder QA Teams
    * Wie sieht ihr Testing Live Cycle aus

---

## Aufgabe 2

* Versuchen Sie folgende Begriffe einzuordnen und Abhängigkeiten untereinander zu erkennen
  * Testing approach
  * Testing levels
  * Testing types, techniques and tactics