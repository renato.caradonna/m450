![TBZ Logo](../../x_gitres/tbz_logo.png)


<!-- TOC -->
* [Eckdaten](#eckdaten)
<!-- TOC -->


---


<img src="./x_gitres/project.png"  width="70%" alt="Testarten">

# Eckdaten

Anforderungen an das Projekt:

* Wählen Sie ein Projekt, welches Ihnen Spass macht
* Es kann auch ein existierendes Projekt sein, welches wenig Test Abdeckung hat
  * Falls die Testabdeckung vorhanden ist, würde man dann eher zusätzliche Features Entwickeln
* Es sollen Unit / Component Tests sichtbar sein
  * Ein Mocking Framework soll verwendet werden
* Code Coverage soll einsehbar sein
* Automatisiert sollen in irgendwelcher Form Test Reports dargestellt werden
* Code Coverage sollte irgendwie als Report ersichtlich sein (Sonar?)
* TDD soll mal ausprobiert werden
* Es sind aktiv pro Team Member 3 Pull Requests einzusehen die aktiv kommentiert, challenged und konstruktive Beiträge haben (von allen Mitglieder der Gruppe)


* Es soll eine kleine Dokumentation erstellt werden
  * Kurze Planung (ohne GANT)
  * Grobe Archtitektur soll visualisiert werden
  * Soll ein kleines [Test Konzept](https://gitlab.com/ch-tbz-it/Stud/m450/m450/-/blob/main/Unterlagen/testkonzept/README.md) vorweisen
  * Kleine Reflexion über TDD und Code Reviews


* Präsentation
  * 10 Minuten 
  * Das Endprodukt soll kurz gezeigt werden
  * Eingehen aus das Thema Testing
  * Reports vorzeigen
  * Reflexion über Code Reviews und TDD
  * Kurzes Fazit