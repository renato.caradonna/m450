![TBZ Logo](../../x_gitres/tbz_logo.png)
# Theorie Prüfung

Die Prüfung umfasst die Lerninhalte von folgenden Themen-Blöcken:

### Grundlagen Testing und Vorgehensmodelle
* Fehler vs Mangel
* Fehlermaskierung
* Kriterien für gute Testfälle
* Testing in Vorgehensmodellen
* Testabdeckung

### Teststrategie
* Testobjekte
* Funktionale und nicht-funktionale Tests
* Abstrakte und konkrete Tests
* BlackBox und WhiteBox Testing

### Testlevels
* Die verschiedenen Testlevels (von Unit-Testing bis zu Acceptance-Test)

### Unit Testing
* Kriterien für gute Unit Tests
* Assertions
* Annotations

Es kommen evtl. noch Fragen zur Automatisierung und zu den Mock-Ups hinzu. Ihre Lehrperson wird Sie darüber informieren.

Sie können sich auf die Prüfung vorbereiten, indem Sie die einzelnen Lerninhalte nochmals repetieren. Achten Sie auch darauf, dass Sie alle Übungen dazu gelöst und mit der Lehrperson besprochen haben.



