![TBZ Logo](../../x_gitres/tbz_logo.png)
# Modulstart

Bei diesem Schritt geht es um das Setup und einige Definitionen, so dass die Arbeit der Lehrpersonen für die Bewertung vereinfacht wird.

## Gitlab oder Github
Erstellen sie als Gruppe ein Repository für ihre Gruppenarbeit. Beide Mitglieder müssen auf das Projekt Zugriff haben und bearbeiten können.
Die Lehrperson teilt ihnen mit wo sie den Link auf ihr Repo ablegen können. Grundsätzlich arbeiten wir an der TBZ mit Gitlab. Es ist aber auch Github möglich.

## Ordnerstruktur Themen und Aufgaben
In diesem Modul erarbeiten wir die Themen in Blöcken. Somit macht es Sinn, wenn Sie diese Blöcke in Ihrem Repository abbilden.
Und zwar wie folgt: für jeden Block wird ein Repository erstellt.

* auswertungen
* automation-testing
* ci-cd-pipeline
* code-reviews
* grundlagen
* schnittstellen
* test-driven-development
* test-levels
* test-umgebungen
* teststrategie
* unit-testing


## Leistungsbeurteilung
In diesem Modul werden drei Noten erstellt. Die erste Note betrifft Ihr Engagement bei den Übungen. Diese Arbeit wird mit 10-20% bewertet.
Die zweite Note besteht aus einer Theorieprüfung und hat 30-40% Gewicht.
Die dritte Note beurteilt Ihr Vorgehen im Projekt, wo das Testen im Fokus steht.






