# Aufgaben

* Arbeiten Sie in Teams
* Bringen Sie die Angular und Spring Boot Applikation bei sich lokal zum laufen.
    * Den Source Code finden Sie
      hier: [Link](https://gitlab.com/ch-tbz-it/Stud/m450/m450/-/blob/main/Unterlagen/automation-testing/spring-boot-angular-basic-lw.zip)

## Übung 1

* Suchen Sie sich nach einem Tool oder nach einer Möglichkeit, die Applikation der REST Schnittstelle des Backends
  automatisiert zu testen. Sie dürfen auch
  kreativ sein.
* Zeigen Sie ihre Lösung

---

## Übung 2

* Wählen Sie ein Frontend Test tool aus und versuchen Sie die Angular Applikation (das GUI) automatisiert zu testen im
  Stile eines End-To-End Tests. 
* Es soll automatisiert in einem Browser ausgeführt werden.
* Zeigen Sie ihre Lösung

---

## Übung 3

* Versuchen Sie mit [JMeter](https://jmeter.apache.org/) das Backend mit grösserem Traffic zu belasten. 
* Erkunden Sie JMeter bezüglich den Funktionalitäten
* Dokumentieren Sie eine Zusammenfassung in einem Markdown-Dokument

---

## Bonus Aufgabe

Leider sind wir Entwickler nicht immer sehr gut darin wenn es darum geht die Zeit zu schätzen um ein Feature zu Implementieren. In dieser Übung haben Sie die Möglichkeit, dies zu trainieren. Da es hier nicht direkt ums Testing geht, ist es als Bonus Übung anzusehen.

Definieren Sie ein Feature welches Sie in der Angular/Spring Boot Applikation implementieren wollen. Dies kann das Frontend (Angular), Backend (Spring Boot) oder beiden Teile involvieren. Definieren Sie das Feature möglichst im Detail in einem Markdown-Dokument. Die Zeit für die Implementation sollte eine Lektion in Anspruch nehmen.

Setzen Sie das Feature um. Dokumentieren Sie wieviel Zeit die Implementation tatsächlich in Anspruch genommen hat. Reflektieren Sie.

Mögliche Feature Ideen:
* Input Validierungen (Backend oder Frontend)
* Error Handling
* Zusätzliche Felder (mit zusätzlichen Datentypen)


---