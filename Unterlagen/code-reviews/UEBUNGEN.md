# Aufgaben

* Bilden Sie zweier Teams

## Aufgabe 1

* Klonen Sie beide jeweils folgendes Repository: https://gitlab.com/module-450/pull-request-exercise (TODO repo muss
  noch verschoben werden)
* Implementieren Sie das Feature des 'Animal Generator's
* Für mehr Informationen schauen Sie in das README.md des Projektes
* Erstellen Sie jeweils einen Pull Request über die gleiche Feature Implementation
* Reviewen Sie sich gegenseitig
* Vergleichen Sie die Inputs zusammen, nachdem beide fertig reviewt haben und diskutieren Sie
* Tragen Sie ihre Erkenntnisse Live per IDE/Gitlab der Klasse vor

---
