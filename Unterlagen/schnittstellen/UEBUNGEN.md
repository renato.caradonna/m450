# Aufgaben

* Bilden Sie zweier Gruppen
* Nehmen Sie das [Addressbuch](02_5_Addressbook_testing_vorgabe.zip) in Betrieb

## Aufgabe 1

* Schreiben Sie Tests für alle Klassen, ohne eine echte MySQL Datenbank benutzen zu müssen
* Schauen Sie, dass Sie auch zusätzliche Annotationen wie @BeforeEach benutzen
* Fangen Sie an mit dem Testen von Addressen, welche Sie erstellen
* Versuchen Sie den Service zu testen indem Sie AddressDAO mit @Mock Annotieren
* Testen Sie ob die Comparator Klasse korrekt sortiert

---

## Aufgabe 2
* Testen Sie den Service zusätzlich mit einer eigenen Mockup Implementation welche AddressDAO implementiert

---

## Aufgabe 3
* Erweitern Sie die Comparator Klasse, so dass nach zusätzlichen Attributen verglichen werden kann
* Testen Sie entsprechend die neue Funktionalität
---